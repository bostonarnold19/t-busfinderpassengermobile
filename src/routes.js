export default [
    {
        path: '/terms-and-condition',
        component: require('./assets/vue/pages/terms-and-condition.vue')
    },
    {
        path: '/map',
        component: require('./assets/vue/map/index.vue')
    },
    {
        path: '/view-bus-route/bus/:plateNumber',
        component: require('./assets/vue/map/selected-bus.vue')
    }
]
